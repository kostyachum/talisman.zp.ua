from django.contrib import admin
from .models import Gallery, GalleryImage, Category
from sorl.thumbnail.admin import AdminImageMixin


class ImageInline(AdminImageMixin, admin.StackedInline):
    model = GalleryImage


class GalleryAdmin(AdminImageMixin, admin.ModelAdmin):
    inlines = [ImageInline]


class CategoryAdmin(admin.ModelAdmin):
    pass


admin.site.register(Gallery, GalleryAdmin)
admin.site.register(Category, CategoryAdmin)
