# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from apps.collections.models import GalleryImage
import urllib
from urlparse import urlparse
from django.core.files import File


def load_images(*args, **kwargs):
    for img in GalleryImage.objects.filter(image_url__isnull=False):
        name = urlparse(img.image_url).path.split('/')[-1]
        content = urllib.urlretrieve(img.image_url)
        img.image.save(name, File(open(content[0])), save=True)


class Migration(migrations.Migration):

    dependencies = [
        ('collections', '0005_auto_20160405_2114'),
    ]

    operations = [
        migrations.RunPython(code=load_images, reverse_code=lambda app, schema_editor: None)
    ]
