# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('collections', '0006_auto_20160405_2123'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gallery',
            name='image_url',
        ),
        migrations.RemoveField(
            model_name='galleryimage',
            name='image_url',
        ),
    ]
