# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('collections', '0004_gallery_description'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='galleryimage',
            name='image_small',
        ),
        migrations.RemoveField(
            model_name='galleryimage',
            name='image_small_url',
        ),
        migrations.AlterField(
            model_name='gallery',
            name='image',
            field=sorl.thumbnail.fields.ImageField(upload_to=b'', null=True, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435 \u0434\u043b\u044f \u043a\u0430\u0440\u0443\u0441\u0435\u043b\u0438', blank=True),
        ),
        migrations.AlterField(
            model_name='galleryimage',
            name='image',
            field=sorl.thumbnail.fields.ImageField(upload_to=b'', null=True, verbose_name='\u0424\u043e\u0442\u043e\u0433\u0440\u0430\u0444\u0438\u044f', blank=True),
        ),
    ]
